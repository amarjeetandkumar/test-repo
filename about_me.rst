About me
===============

My name is Mauricio Acosta and I'm a Data Scientist/Physicist/Biomedical Engineer who currently works as a Computational Physics Associate at the Miami Cancer Institute of Baptist Health South Florida. My interests are in the application of machine learning and artificial intelligence in healthcare and biomedical applications. Currently my work has primarily been in automatization of clinical workflows related to quality control and treatment planning in proton physics, but am excited for the future integration of ML/AI to enhance clinical workflows.


.. figure:: my_pic_head.png
   :alt: my_pic_head.png
   :align: center
   :scale: 25 %
